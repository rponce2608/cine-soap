package pe.com.cine.services.cinews.ws.impl;

import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.xml.ws.BindingType;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.soap.SOAPBinding;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import pe.com.cine.entity.Genero;
import pe.com.cine.entity.Pelicula;
import pe.com.cine.entity.ws.types.BuscarPorCodigoRequest;
import pe.com.cine.entity.ws.types.BuscarPorCodigoResponse;
import pe.com.cine.entity.ws.types.BuscarPorGeneroRequest;
import pe.com.cine.entity.ws.types.BuscarPorGeneroResponse;
import pe.com.cine.entity.ws.types.BuscarRequest;
import pe.com.cine.entity.ws.types.BuscarResponse;
import pe.com.cine.entity.ws.types.GeneroType;
import pe.com.cine.entity.ws.types.ListaGeneroType;
import pe.com.cine.entity.ws.types.ListaPeliculaType;
import pe.com.cine.entity.ws.types.PeliculaType;
import pe.com.cine.services.GeneroServiceImpl;
import pe.com.cine.services.IGeneroService;
import pe.com.cine.services.IPeliculaService;
import pe.com.cine.services.PeliculaServiceImpl;
import pe.com.cine.services.cinews.ws.CineWS;

@WebService(serviceName = "cineWS", targetNamespace = "http://cine.com.pe/services/cinews/ws", portName = "cineWS", endpointInterface = "pe.com.cine.services.cinews.ws.CineWS", wsdlLocation = "META-INF/wsdl/cineWS.wsdl")
@BindingType(SOAPBinding.SOAP11HTTP_BINDING)
public class CineWSImpl implements CineWS {

	private static final Logger logger = Logger.getLogger(CineWSImpl.class.getName());

	@Resource
	private WebServiceContext context;

	@Override
	public BuscarResponse buscarDatos(BuscarRequest request) {
		logger.info("[buscarDatos] =========== INICIO ===========");
		BuscarResponse response = new BuscarResponse();
		try {
			ServletContext servletContext = (ServletContext) context.getMessageContext()
					.get("javax.xml.ws.servlet.context");
			WebApplicationContext webApplicationContext = WebApplicationContextUtils
					.getRequiredWebApplicationContext(servletContext);

			IGeneroService generoService = (GeneroServiceImpl) webApplicationContext.getAutowireCapableBeanFactory()
					.getBean("generoServiceImpl");

			List<Genero> lta = generoService.find(request.getIdGenero());

			if (lta != null && lta.size() > 0) {
				ListaGeneroType ltaType = new ListaGeneroType();

				for (Genero genero : lta) {
					GeneroType gen = new GeneroType();
					gen.setIdGenero(genero.getIdGenero());
					gen.setNombre(genero.getNombre());

					ltaType.getGenero().add(gen);
				}
				response.setListaGenero(ltaType);
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception: ", e);
		}
		return response;
	}

	@Override
	public BuscarPorCodigoResponse buscarDatosPeliculaPorCodigo(BuscarPorCodigoRequest requestPorCodigo) {
		logger.info("[buscarDatosPeliculaPorCodigo] =========== INICIO ===========");
		BuscarPorCodigoResponse response = new BuscarPorCodigoResponse();
		try {
			ServletContext servletContext = (ServletContext) context.getMessageContext()
					.get("javax.xml.ws.servlet.context");
			WebApplicationContext webApplicationContext = WebApplicationContextUtils
					.getRequiredWebApplicationContext(servletContext);

			IPeliculaService peliculaService = (PeliculaServiceImpl) webApplicationContext
					.getAutowireCapableBeanFactory().getBean("peliculaServiceImpl");

			List<Pelicula> lta = peliculaService.buscarPeliculaporCodigo(requestPorCodigo.getIdPelicula());

			if (lta != null && lta.size() > 0) {
				ListaPeliculaType ltaType = new ListaPeliculaType();

				for (Pelicula pelicula : lta) {					
					PeliculaType pel = new PeliculaType();
					GeneroType gen=new GeneroType();
					gen.setIdGenero(pelicula.getGenero().getIdGenero());
					gen.setNombre(pelicula.getGenero().getNombre());					

					pel.setIdPelicula(pelicula.getIdPelicula());
					pel.setGenero(gen);
					pel.setTituloLocal(pelicula.getTitulo_local());
					
					ltaType.getPelicula().add(pel);
				}
				response.setListaPelicula(ltaType);
			}

		} catch (Exception e) {
			logger.error("Exception: ", e);
		}
		return response;
	}

	@Override
	public BuscarPorGeneroResponse buscarDatosPeliculaPorGenero(BuscarPorGeneroRequest requestPorGenero) {
		logger.info("[buscarDatosPeliculaPorGenero] =========== INICIO ===========");
		BuscarPorGeneroResponse response = new BuscarPorGeneroResponse();
		try {
			ServletContext servletContext = (ServletContext) context.getMessageContext()
					.get("javax.xml.ws.servlet.context");
			WebApplicationContext webApplicationContext = WebApplicationContextUtils
					.getRequiredWebApplicationContext(servletContext);

			IPeliculaService peliculaService = (PeliculaServiceImpl) webApplicationContext
					.getAutowireCapableBeanFactory().getBean("peliculaServiceImpl");

			List<Pelicula> lta = peliculaService.buscarPeliculaporGenero(requestPorGenero.getIdGenero());

			if (lta != null && lta.size() > 0) {
				ListaPeliculaType ltaType = new ListaPeliculaType();

				for (Pelicula pelicula : lta) {
					PeliculaType pel = new PeliculaType();
					GeneroType gen=new GeneroType();
					gen.setIdGenero(pelicula.getGenero().getIdGenero());
					gen.setNombre(pelicula.getGenero().getNombre());					

					pel.setIdPelicula(pelicula.getIdPelicula());
					pel.setGenero(gen);
					pel.setTituloLocal(pelicula.getTitulo_local());
					
					ltaType.getPelicula().add(pel);
				}
				response.setListaPelicula(ltaType);
			}

		} catch (Exception e) {
			logger.error("Exception: ", e);
		}
		return response;
	}

}
