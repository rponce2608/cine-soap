package pe.com.cine.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.cine.entity.Pelicula;
import pe.com.cine.repository.PeliculaDao;

@Service
public class PeliculaServiceImpl implements IPeliculaService {

	private static final Logger logger = Logger.getLogger(GeneroServiceImpl.class.getName());

	@Autowired
	private PeliculaDao peliculaDao;

	@Override
	public List<Pelicula> buscarPeliculaporCodigo(String idPelicula) {
		List<Pelicula> ltaPelicula = null;

		if (idPelicula != null) {
			ltaPelicula = new ArrayList<Pelicula>();
			Pelicula pelicula = (Pelicula) peliculaDao.buscarPeliculaporCodigo(idPelicula);
			if (pelicula != null) {
				ltaPelicula.add(pelicula);
			}

		} else {
			ltaPelicula = peliculaDao.findAll();
		}

		return ltaPelicula;
	}

	@Override
	public List<Pelicula> buscarPeliculaporGenero(String idGenero) {
		List<Pelicula> ltaPelicula = null;

		if (idGenero != null) {
			ltaPelicula = peliculaDao.buscarPeliculaporGenero(idGenero);
		} else {
			ltaPelicula = peliculaDao.findAll();
		}

		return ltaPelicula;
	}

	@Override
	public List<Pelicula> listarPeliculas() {
		List<Pelicula> ltaPelicula = null;
		ltaPelicula = peliculaDao.findAll();

		return ltaPelicula;
	}

}
