package pe.com.cine.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.cine.entity.Genero;
import pe.com.cine.repository.GeneroDao;

@Service
public class GeneroServiceImpl implements IGeneroService {

	private static final Logger logger = Logger.getLogger(GeneroServiceImpl.class.getName());

	@Autowired
	private GeneroDao generoDao;

	@Override
	public List<Genero> find(String idGenero) {
		List<Genero> ltaGenero = null;

		if (idGenero != null) {
			ltaGenero = new ArrayList<Genero>();
			Genero genero = (Genero) generoDao.buscarGeneroporCodigo(idGenero);
			if (genero != null) {
				ltaGenero.add(genero);
			}
		} else {
			ltaGenero = generoDao.findAll();
		}

		return ltaGenero;

	}

}
