
package pe.com.cine.entity.ws.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para PeliculaType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PeliculaType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idPelicula" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="genero" type="{http://cine.com.pe/entity/ws/types}GeneroType"/&gt;
 *         &lt;element name="titulo_local" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PeliculaType", propOrder = {
    "idPelicula",
    "genero",
    "tituloLocal"
})
public class PeliculaType {

    @XmlElement(required = true)
    protected String idPelicula;
    @XmlElement(required = true)
    protected GeneroType genero;
    @XmlElement(name = "titulo_local", required = true, nillable = true)
    protected String tituloLocal;

    /**
     * Obtiene el valor de la propiedad idPelicula.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdPelicula() {
        return idPelicula;
    }

    /**
     * Define el valor de la propiedad idPelicula.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdPelicula(String value) {
        this.idPelicula = value;
    }

    /**
     * Obtiene el valor de la propiedad genero.
     * 
     * @return
     *     possible object is
     *     {@link GeneroType }
     *     
     */
    public GeneroType getGenero() {
        return genero;
    }

    /**
     * Define el valor de la propiedad genero.
     * 
     * @param value
     *     allowed object is
     *     {@link GeneroType }
     *     
     */
    public void setGenero(GeneroType value) {
        this.genero = value;
    }

    /**
     * Obtiene el valor de la propiedad tituloLocal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTituloLocal() {
        return tituloLocal;
    }

    /**
     * Define el valor de la propiedad tituloLocal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTituloLocal(String value) {
        this.tituloLocal = value;
    }

}
