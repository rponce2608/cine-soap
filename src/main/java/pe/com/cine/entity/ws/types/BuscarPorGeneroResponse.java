
package pe.com.cine.entity.ws.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="listaPelicula" type="{http://cine.com.pe/entity/ws/types}ListaPeliculaType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "listaPelicula"
})
@XmlRootElement(name = "buscarPorGeneroResponse")
public class BuscarPorGeneroResponse {

    @XmlElement(required = true)
    protected ListaPeliculaType listaPelicula;

    /**
     * Obtiene el valor de la propiedad listaPelicula.
     * 
     * @return
     *     possible object is
     *     {@link ListaPeliculaType }
     *     
     */
    public ListaPeliculaType getListaPelicula() {
        return listaPelicula;
    }

    /**
     * Define el valor de la propiedad listaPelicula.
     * 
     * @param value
     *     allowed object is
     *     {@link ListaPeliculaType }
     *     
     */
    public void setListaPelicula(ListaPeliculaType value) {
        this.listaPelicula = value;
    }

}
